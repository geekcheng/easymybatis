#set($jq="$")
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE  mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${context.namespace}">
    <resultMap id="baseResultMap" type="${context.className}">
    #foreach($column in $allColumns)
        <result column="${column.columnName}" property="${column.javaFieldName}" ${column.javaTypeProperty} ${column.typeHandlerProperty} ${column.jdbcTypeProperty} />
    #end
    </resultMap>
    
    <!-- 表字段 -->
    <sql id="baseColumns">
    	#set ($i=0) 
        #foreach($column in $columns)                         
        	#if($i > 0),#end t.${column.columnName}
			#set($i=$i +1)             
        #end
    </sql>
    
    <!-- 聚合查询 -->
    <select id="findProjection" parameterType="net.oschina.durcframework.easymybatis.query.projection.ProjectionQuery" resultType="java.util.HashMap">
		SELECT
			<include refid="common.projectionColumn"/>
		FROM ${table.tableName} t
		<include refid="common.join" />
        <where>
            <include refid="common.condition" />
            #if(${table.hasLogicDeleteColumn})
              AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
            #end
        </where>  
		<include refid="common.groupBy"/>
		<include refid="common.having"/>
		<if test="orderable">
			ORDER BY ${jq}{order}
		</if>
	</select>
	
	<!-- 根据条件查询记录 -->
    <select id="find" parameterType="net.oschina.durcframework.easymybatis.query.Pageable"
		resultMap="baseResultMap">
		SELECT *
		FROM 
		(
				SELECT tt.*, ROWNUM AS rowno
		    FROM ( 
						SELECT 
							<include refid="common.sqlColumns" />
						FROM ${table.tableName} t
						<include refid="common.join" />
                        <where>
                            <include refid="common.condition" />
                            #if(${table.hasLogicDeleteColumn})
                              AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
                            #end
                        </where>
						<include refid="common.orderBy" />
				) tt
				<if test="!isQueryAll">
				WHERE ROWNUM &lt;= (#{start}  + #{limit})
				</if>
		) table_alias
		<if test="!isQueryAll">
		WHERE table_alias.rowno &gt;= (#{start}+1)
		</if>
	</select>

	<!-- 根据条件查询记录数 -->
    <select id="countTotal" parameterType="net.oschina.durcframework.easymybatis.query.Pageable"
		resultType="long">
		SELECT ${countExpression} FROM ${table.tableName} t
		<include refid="common.join" />
        <where>
            <include refid="common.condition" />
            #if(${table.hasLogicDeleteColumn})
              AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
            #end
        </where>
    </select>

	<!-- 根据主键获取单条记录 -->
    <select id="get" resultMap="baseResultMap" parameterType="${context.className}">
		SELECT 
        	<include refid="baseColumns" />
		FROM ${table.tableName} t
		WHERE t.${pk.columnName} = #{${pk.javaFieldName}}
		#if(${table.hasLogicDeleteColumn})
          AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
        #end
		AND ROWNUM = 1
	</select>
    
    <!-- 根据条件获取单条记录 -->
    <select id="getByExpression" resultMap="baseResultMap" parameterType="net.oschina.durcframework.easymybatis.query.Pageable">
		SELECT
			<include refid="common.sqlColumns" />			
		FROM ${table.tableName} t
		<include refid="common.join" />
        <where>
            <include refid="common.condition" />
            #if(${table.hasLogicDeleteColumn})
              AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
            #end
        </where>
		<include refid="common.orderBy" />
		AND ROWNUM = 1
	</select>
	
	<!-- 根据属性获取单条记录 -->
	<select id="getByProperty" resultMap="baseResultMap">
		SELECT
			<include refid="baseColumns" />
		FROM ${table.tableName} t
		WHERE t.${jq}{column} = #{value}
		#if(${table.hasLogicDeleteColumn})
          AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
        #end
		AND ROWNUM = 1
	</select>
	
	<!-- 根据属性获取多条记录 -->
	<select id="listByProperty" resultMap="baseResultMap">
		SELECT
			<include refid="baseColumns" />
		FROM ${table.tableName} t
		WHERE t.${jq}{column}= #{value}
		#if(${table.hasLogicDeleteColumn})
          AND t.${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
        #end
	</select>
	

	<!-- 保存,保存全部字段 -->
    <insert id="save" parameterType="${context.className}"
#if(${pk.isIdentity})
    keyProperty="${pk.javaFieldName}" keyColumn="${pk.columnName}" useGeneratedKeys="true"
#end
    >
     #if(${pk.isUuid})
	<selectKey keyProperty="${pk.javaFieldName}" resultType="String" order="BEFORE">
            SELECT SYS_GUID() FROM DUAL
    </selectKey>
#end
	INSERT INTO ${table.tableName}
         (
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end ${column.columnName}
#set($i=$i +1)
            #end          
        #end
          )
	VALUES (
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end ${column.mybatisInsertValue}
#set($i=$i +1)
            #end          
        #end
 
        )
	</insert>
    
    <!-- 保存,保存不为NULL的字段 -->
    <insert id="saveIgnoreNull" parameterType="${context.className}"
    #if(${pk.isIdentity})
        keyProperty="${pk.javaFieldName}" keyColumn="${pk.columnName}" useGeneratedKeys="true"
    #end
        >
         #if(${pk.isUuid})
	<selectKey keyProperty="${pk.javaFieldName}" resultType="String" order="BEFORE">
            SELECT SYS_GUID() FROM DUAL
    </selectKey>
#end
        INSERT INTO ${table.tableName}
    
        <trim prefix="(" suffix=")" suffixOverrides=",">	 

            #foreach($column in $columns) 
                #if(!${column.isIdentityPk}) 
                    <if test="${column.javaFieldName} != null">
                   ${column.columnName},
                    </if>
                #end          
            #end
        </trim>
        <trim prefix="VALUES (" suffix=")" suffixOverrides=",">            
            #foreach($column in $columns) 
                #if(!${column.isIdentityPk})  
                    <if test="${column.javaFieldName} != null" >
                    ${column.mybatisInsertValue},                    
                    </if>
                #end          
            #end
        </trim>
    </insert>

	<!-- 批量保存,保存全部字段 -->
    <insert id="saveBatch">
		INSERT INTO ${table.tableName} 
		(
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end ${column.columnName}
#set($i=$i +1)
            #end          
        #end
          )
		VALUES 
		<foreach collection="entitys" item="entity"  separator=",">
        ( #set ($i=0)
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
         	#if($i > 0),#end ${column.mybatisInsertValuePrefix}
         	#set($i=$i +1)
            #end          
        #end
        )
		</foreach>
	</insert>


	 <!-- 批量保存,保存指定字段 -->
    <insert id="saveBatchWithColumns">
		INSERT INTO ${table.tableName}
		<foreach collection="columns" item="column" open="(" close=")" separator=",">
			${jq}{column.columnName}
		</foreach>
		VALUES 
		<foreach collection="entitys" item="entity"  separator=",">
        ( 
        	<foreach collection="columns" item="column"  separator=",">
        		#{entity.${column.alias} ${column.typeHandler}}
        	</foreach>
        )
        </foreach>
	</insert>
	
	<!-- 批量保存(兼容),保存全部字段 -->
    <insert id="saveMulti">
		INSERT INTO ${table.tableName} 
		(
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end ${column.columnName}
#set($i=$i +1)
            #end          
        #end
          )
		<foreach collection="entitys" item="entity"  separator="UNION ALL">
        SELECT  #set ($i=0)
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
         	#if($i > 0),#end ${column.mybatisInsertValuePrefix}
         	#set($i=$i +1)
            #end          
        #end
        FROM dual
		</foreach>
	</insert>
	
	<!-- 批量保存(兼容),保存指定字段 -->
    <insert id="saveMultiWithColumns">
		INSERT INTO ${table.tableName}
		<foreach collection="columns" item="column" open="(" close=")" separator=",">
			${jq}{column.columnName}
		</foreach>
		<foreach collection="entitys" item="entity"  separator="UNION ALL">
        SELECT  
        	<foreach collection="columns" item="column"  separator=",">
        		#{entity.${column.alias} ${column.typeHandler}}
        	</foreach>
        FROM dual
        </foreach>
	</insert>

	<!-- 更新,更新全部字段 -->
    <update id="update" parameterType="${context.className}">
    UPDATE ${table.tableName}
     <set>		
     #foreach($column in $columns) 
         #if(!${column.isPk})               
         ${column.columnName}=${column.mybatisUpdateValue},
         #end          
     #end
     </set>	
    WHERE ${pk.columnName} = #{${pk.javaFieldName}}
    #if(${table.hasVersionColumn})
		AND ${table.versionColumn.columnName} = #{${table.versionColumn.javaFieldName}} 
    #end
    </update>
    
    <!-- 更新不为NULL的字段 -->
    <update id="updateIgnoreNull" parameterType="${context.className}">
    UPDATE ${table.tableName}
    <set>
    	#foreach($column in $columns) 
            #if(!${column.isPk})  
                <if test="${column.javaFieldName} != null" >
                ${column.columnName}=${column.mybatisUpdateValue},
                </if>
            #end          
        #end
    </set>
    WHERE ${pk.columnName} = #{${pk.javaFieldName}}
    #if(${table.hasVersionColumn})
		AND ${table.versionColumn.columnName} = #{${table.versionColumn.javaFieldName}} 
    #end
    </update>
    
    <!-- 根据指定条件更新不为NULL的字段 -->
    <update id="updateIgnoreNullByExpression">
    UPDATE ${table.tableName}
    <set>
    	#foreach($column in $columns) 
            #if(!${column.isPk})  
                <if test="entity.${column.javaFieldName} != null" >
                ${column.columnName}=${column.mybatisUpdateValuePrefix},                 
                </if>
            #end          
        #end
    </set>
    <include refid="common.updateWhere" />
    </update>	
	
	<!-- 根据主键删除记录 -->
	<delete id="del" parameterType="${context.className}">
		#if(${table.hasLogicDeleteColumn})            
            UPDATE ${table.tableName}
            <set>
                #foreach($column in $columns) 
                    #if(${column.isPk})  
                    #elseif(${column.isLogicDelete})
                        ${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicDeleteValueString},
                    #else
                        <if test="${column.javaFieldName} != null" >
                            ${column.columnName}=${column.mybatisUpdateValue},
                        </if>
                    #end          
                #end
            </set>
            WHERE ${pk.columnName} = #{${pk.javaFieldName}}
            AND ${table.logicDeleteColumn.columnName} = ${table.logicDeleteColumn.logicNotDeleteValueString}
            #if(${table.hasVersionColumn})
                AND ${table.versionColumn.columnName} = #{${table.versionColumn.javaFieldName}} 
            #end    
       #else
            DELETE FROM ${table.tableName}
            WHERE ${pk.columnName} = #{${pk.javaFieldName}}
       #end
	</delete>    
    
    <!-- 根据条件删除记录 -->
    <delete id="delByExpression" parameterType="net.oschina.durcframework.easymybatis.query.Pageable">
		DELETE FROM ${table.tableName}
		<include refid="common.where" />
	</delete>
	
	<!--_ext_mapper_-->
    
    <!--_global_vm_-->

</mapper>